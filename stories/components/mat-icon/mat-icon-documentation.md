A component used to display a [Material Icon](https://fonts.google.com/icons). Can be useful when used in wcs-grid.

When used directly in another Web Component, the global CSS stylesheet doesn't apply to material-icons.
In this case you can use the Mat-icon with its integrated material stylesheet.
